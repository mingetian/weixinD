const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [month, day].map(formatNumber).join('/') + ' ' + [hour, minute].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}


//处理钱数，和分数
const dealNum = function (num,cs) {
  if (cs == "score") {
    return num/10;
  }
  num = num.toString();
  if (num.length == 1) {
    num = '0.0' + num;
  } else if (num.length == 2) {
    num = '0.' + num;
  } else {
    num = num.slice(0, num.length - 2) + '.' + num.slice(num.length - 2);
  }
  return num;
};

module.exports = {
  formatTime: formatTime,
  dealNum: dealNum
}
