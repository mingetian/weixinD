import server from './utils/https'
const http = server.http;
let tempcode = null;
//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || [];
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    let that = this;
    //检测登录态
    wx.checkSession({
      success: function () {
        //session 未过期，并且在本生命周期一直有效
        wx.getUserInfo({
          withCredentials: true,
          success: res => {
            // 可以将 res 发送给后台解码出 unionId
            that.globalData.userInfo = res.userInfo
            // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
            // 所以此处加入 callback 以防止这种情况
            if (that.userInfoReadyCallback) {
              that.userInfoReadyCallback(res)
            }
          }
        })


        //方便测试用
         //that.login()
      },
      fail: function () {
        //登录态过期
        // 登录
  
        that.login()

      }
    })
    
    




   
  },
  //登陆
  login: function(){
    let that = this;
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        var code = res.code;
        if (code) {
          tempcode = code;
          that.getInfo(code);
        } else {
          console.log('获取用户登录态失败：' + res.errMsg);
        }
      }
    })
  },
  //用户授权
  
  getInfo: function (code){
    console.log("ppppp")
    // 获取用户信息
    let that = this;
    if (wx.getSetting){
      wx.getSetting({
        success: res => {
          if (res.authSetting['scope.userInfo']) {
            // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
            wx.getUserInfo({
              withCredentials: true,
              success: res => {
                // 可以将 res 发送给后台解码出 unionId
                that.globalData.userInfo = res.userInfo
                that.sendInfo(res, code);
                // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                // 所以此处加入 callback 以防止这种情况
                if (that.userInfoReadyCallback) {
                  that.userInfoReadyCallback(res)
                }
              }
            })
          }
          if (!res.authSetting['scope.userInfo']) {
            wx.authorize({
              scope: 'scope.userInfo',
              success() {
                // 用户已经同意小程序使用用户信息功能，后续调用 wx.startRecord 接口不会弹窗询问
                wx.getUserInfo({
                  success: res => {
                    // 可以将 res 发送给后台解码出 unionId
                    that.globalData.userInfo = res.userInfo
                    //发送服务器
                    that.sendInfo(res, code);
                    // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                    // 所以此处加入 callback 以防止这种情况
                    if (that.userInfoReadyCallback) {
                      that.userInfoReadyCallback(res)
                    }
                  }
                })
              },
              fail: function (res) { //用户点了“拒绝”
                // 向用户提示需要权限才能继续
                
                if (that.openInfocb) {
                  that.openInfocb(false);
                }

              }
            })
          }
        }
      })
    }else{
      wx.getUserInfo({
        success: res => {
          that.globalData.userInfo = res.userInfo
          that.sendInfo(res,code);
          if (that.userInfoReadyCallback) {
            that.userInfoReadyCallback(res)
          }
        },
        fail: function () {
          //兼容以前授权版本
          console.log(that, 1111)
          if (that.openInfocb) {
            that.openInfocb(false);
          }
        }
      })
    }
   
  },
  //发送用户信息给服务器存储
  sendInfo: function(res,code){
    let that = this;
    wx.showLoading({
      title: '登陆中...',
    })
    wx.request({
      url: http + '/wechat/api/login',
      data: {
        fun: 'login',
        code: code,
        userName: res.userInfo.nickName,
        userPic: res.userInfo.avatarUrl
      },
      success: function (res) {
          wx.hideLoading();
          wx.setStorage({
            key: '3rd_session', 
            data: res.data.data,
            success: function(){
              console.log(88888)
              that.getMoneycb && that.getMoneycb(res.data.data);
            }
          });
          

      
      },
      fail: function(){
        console.log("登陆失败")
      }
    })
  },
  dealFail(){
    let that = this;
     if (wx.openSetting){
          wx.openSetting({
            success(res){
              if (res.authSetting["scope.userInfo"]) {
                // 如果成功打开授权
                wx.getUserInfo({
                  success: res => {
                    // 可以将 res 发送给后台解码出 unionId
                    that.login();
                    that.globalData.userInfo = res.userInfo
                    if (that.openInfocb) {
                      that.openInfocb(true);
                    }
                  
                    // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                    // 所以此处加入 callback 以防止这种情况
                    if (that.userInfoReadyCallback) {
                      that.userInfoReadyCallback(res)
                    }
                  }
                })
              } else {
                // 如果用户依然拒绝授权
              }
            },
            fail: function () { //调用失败，授权登录不成功
              console.log("openSetting调用失败")
              
            }
          })
        }else{

        }
    // wx.showModal({
    //   "title": "提示",
    //   "content": "取消授权将不能使用小程序",
    //   "confirmText": "确认",
    //   "showCancel": false,
    //   "success": function (res) {
       
        
        
    //   }
    // }) 
  },
  globalData: {
    userInfo: null,
    songname:'',
    songId:'',
  }
})