// pages/record/record.js
const app = getApp()
import server from '../../utils/https'
import util from '../../utils/util'
const http = server.http;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    btnIndex: 1,
    userInfo:{},
    listData: [
      { "code": "01", "text": "text1" },
      { "code": "02", "text": "text2" },
      { "code": "03", "text": "text3"},
    ],
    isLogin: false
  },
  login: function(){
    //登录部分代码


  },
  chageRecord: function(e){
    const index = e.currentTarget.dataset.index;
    wx.showLoading({
      title: '正在加载...',
    })
    if(index==1){
      this.getOutRecord();
    }else if(index==2){
      this.getInnerRecord();
    }else{
      this.getStrealRecord();
    }
    this.setData({
      btnIndex: index
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //获取我发出的数据
    wx.showLoading({
      title: '正在加载中...',
    })
    this.getOutRecord();
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },

  //我发出的记录
  getOutRecord: function(){
    let that = this;
    that.setData({
      infoTotal: {
        totalMoney: '0.00',
        totalNum: 0
      }
    })
    wx.getStorage({
      key: '3rd_session',
      success: function(res) {
        console.log(res,'请求发出记录')
        wx.request({
          url: http + '/wechat/redpacket/order',
          data: {
            fun: 'handOutRed',
            fid: res.data.id,
            sessionKey: res.data.sessionKey,
            openId: res.data.openId,
          },
          method: 'GET',
          success: function (res) {
            wx.hideLoading();
            let totalMoney = 0;
            if (res.statusCode == 200 && res.data.status == 200) {
              res.data.data.list.map((v,i)=>{
                v.tm = util.formatTime(new Date(v.tm*1000));
                v.money = util.dealNum(v.money);
              })
              totalMoney += parseFloat(res.data.data.sumMoney);
             
              console.log(res.data.data.list,'total')
              that.setData({
                outRecord: res.data.data.list,
                infoTotal:{
                  totalNum: res.data.data.count,
                  totalMoney: util.dealNum(totalMoney)
                }
              })
            }

          },
          fail: function () {

          }
        })
      }
    })
  },
  //我收到的
  getInnerRecord: function () {
    let that = this;
    that.setData({
      infoTotal: {
        totalMoney: '0.00',
        totalNum: 0
      }
    })
    wx.getStorage({
      key: '3rd_session',
      success: function (res) {
        console.log(res, '请求收到记录')
        wx.request({
          url: http + '/wechat/redpacket/orderSing',
          data: {
            fun: 'handInRed',
            fid: res.data.id,
            sessionKey: res.data.sessionKey,
            openId: res.data.openId,
          },
          method: 'GET',
          success: function (res) {
            wx.hideLoading();
            let totalMoney = 0;
            if (res.statusCode == 200 && res.data.status == 200) {
              res.data.data.list.map((v, i) => {
                v.tm = util.formatTime(new Date(v.tm*1000));
                v.money = util.dealNum(v.money);
                
              })
              totalMoney += parseFloat(res.data.data.sumMoney);
              that.setData({
                innerRecord: res.data.data.list,
                infoTotal: {
                  totalNum: res.data.data.count,
                  totalMoney: util.dealNum(totalMoney)
                }
              })
            }

          },
          fail: function () {

          }
        })
      }
    })
  },
  //偷听的
  getStrealRecord: function () {
    let that = this;
    this.setData({
      infoTotal: {
        totalMoney: '0.00',
        totalNum: 0
      }
    })
    
    wx.getStorage({
      key: '3rd_session',
      success: function (res) {
        console.log(res, '请求发出记录')
        wx.request({
          url: http + '/wechat/redpacket/orderSteal',
          data: {
            fun: 'handStealRed',
            fid: res.data.id,
            sessionKey: res.data.sessionKey,
            openId: res.data.openId,
          },
          method: 'GET',
          success: function (res) {
            wx.hideLoading();
            let totalMoney = 0;
            if (res.statusCode == 200 && res.data.status == 200) {
              console.log(res.data.data);
              res.data.data.list.map((v, i) => {
                v.tm = util.formatTime(new Date(v.tm*1000));
                v.money = util.dealNum(v.money);
                
              })
              totalMoney += parseFloat(res.data.data.sumMoney);
              that.setData({
                stealRecord: res.data.data.list,
                infoTotal: {
                  totalNum: res.data.data.count,
                  totalMoney: util.dealNum(totalMoney)
                }
              })
            }

          },
          fail: function () {

          }
        })
      }
    })
  },
  //去领红包页面
  goGetMoneyPage: function(e){
    wx.navigateTo({
      url: '/pages/getmoney/getmoney?rpid=' + e.currentTarget.dataset.rpid
    })


  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },


})