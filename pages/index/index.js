//index.js
//获取应用实例
import server from '../../utils/https';
var order = ['red', 'yellow', 'blue', 'green', 'red']
const app = getApp()
const http = server.http;
Page({
  data: {
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    isUserOpen: true,
    imgUrls: [
      '/images/banner0.jpg',
      '/images/banner1.jpg',
      '/images/banner2.jpg',
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    toView: 'red',
    scrollTop: 100,
    list: [["世纪家政", "擦车服务"], ["世纪家政", "擦车服务"], ["世纪家政", "擦车服务"], ["世纪家政", "擦车服务"], ["世纪家政", "擦车服务"], ["世纪家政", "擦车服务"]],
    hot: ["世纪家政擦车服务", "世纪家政擦车服务", "世纪家政擦车服务", "世纪家政擦车服务", "世纪家政擦车服务", "世纪家政擦车服务"],
  },

  goDetail: function () {
    wx.navigateTo({
      url: '/pages/detailhome/detailhome'
    })
  }, 








  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  }, 
  //返回时候显示歌名
  onShow: function(){
    this.setData({
      songname: app.globalData.songname,
      songId: app.globalData.songId,
    })
  },
  getPhoneNumber: function (e) {
    console.log(e.detail.errMsg)
    console.log(e.detail.iv)
    console.log(e.detail.encryptedData)
  } ,
  onLoad: function () {
    let that = this;
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.openInfocb = (res) => {
        this.setData({
          isUserOpen: res
        });
      }
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      app.openInfocb = (res) => {
        this.setData({
          isUserOpen: res
        });
      }
      app.userInfoReadyCallback = res => {
        app.openInfocb = (res) => {
          this.setData({
            isUserOpen: res
          });
        }
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    }
  },
  //兼容1.1.1
  callBefore: function (res){
    this.setData({
      userInfo: res.userInfo,
      hasUserInfo: true
    })
  },
  getUserInfo: function(e) {
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },

  //选择歌曲
  selectSong(){
    wx.navigateTo({
      url: '/pages/selectmusic/selectmusic',
    })
  },

  //处理输入框
  bindKeyMoney: function (e) {
    let val = e.detail.value.replace(/\.+|^0+/g,'');
    if (val - 0 < 1 && e.detail.value!=''){
      wx.showModal({
        title: '提示',
        content: '红包金额不能小于1元呢',
        showCancel: false
      })
      
    }
    if (val - 0 > 50000) {
      wx.showModal({
        title: '提示',
        content: '红包金额不能大于5万元',
        showCancel: false
      })
      val = 50000;

    }
    this.setData({
      inputMoney: val,
      tips: val==0?'0.00':val*0.02
    })

  },
  bindKeyNum: function (e) {
    let val = e.detail.value.replace(/\.+|^0+/g, '');
    if (val - 0 < 1 && e.detail.value != '') {
      wx.showModal({
        title: '提示',
        content: '红包个数不能小于1，并且为整数，亲。',
        showCancel: false
      })
    }
    this.setData({
      inputNum: val
    })
  },
  //塞红包
  giveMoney: function(){
    //判断上边文本框和选择歌曲是否为空

    //不为空调起微信支付接口
    let { inputMoney, inputNum, songname, songId} = this.data;
      if (songname == "") {
        wx.showModal({
          title: '提示',
          content: '请选择一首歌',
          showCancel: false
        })
  
        return false;
      }
      if (inputMoney==''){
        wx.showModal({
          title: '提示',
          content: '请填写红包金额',
          showCancel: false
        })
    
        return false;
      }
      if (inputNum == "" && inputNum-0<1) {
        wx.showModal({
          title: '提示',
          content: '请填写红包个数',
          showCancel: false
        })
   
        return false;
      }
      if ((inputNum - 0) / (inputMoney - 0)>10){
        wx.showModal({
          title: '提示',
          content: '红包个数不能大于红包金额的10倍呢',
          showCancel: false
        })
        return false;
      }
      var that = this;
      wx.getStorage({
        key: "3rd_session",
        success: function(res){
          //创建订单
          wx.showLoading();
          console.log(res);
          wx.request({
            url: http + '/wechat/redpacket/order',
            data: {
              fun: 'share',
              money: inputMoney,
              num: inputNum,
              openId: res.data.openId,
              singerId: songId,
              sessionKey: res.data.sessionKey
            },
            method:'GET',
            success: function(res){
              wx.hideLoading();
              if(res.statusCode==200&&res.data.status==200){
                that.paywx(res.data.data);
              }else{
                console.log(res,11111)
              }
              
            }
          })
        },
        fail: function(){
          wx.hideLoading();
        }
      })
  },
 //授权
  openS: function () {
    app.dealFail();
  },
  //调微信支付
  paywx: function(res){
    let that = this;
    wx.requestPayment({
      timeStamp: res.timeStamp.toString(),
      nonceStr: res.nonceStr,
      package: 'prepay_id=' + res.prepay_id,
      signType: res.signType,
      paySign: res.paySign,
      success: function (respone) {
        console.log(6666)
          wx.redirectTo({
            url: '/pages/sharepage/sharepage?rpid=' + res.redPacketId,
          })
          app.globalData.songname = '';
          app.globalData.songId = '';
          that.setData({
            inputMoney: '',
            inputNum: ''
          })
      },
      fail: function () {
       
        
      },
      complete: function (comp) {

      }
    })
    
  }

})
