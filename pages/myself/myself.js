// pages/withdrawals/withdrawals.js
import server from '../../utils/https'
const http = server.http;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mymoney:'0.00',
    outVal:'',

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getMyMoney();
    
  },
  //获取我的金额
  getMyMoney: function(){
    let that = this;
    wx.getStorage({
      key: "3rd_session",
      success: function (res) {
        wx.request({
          url: http + "/wechat/account/user",
          data: {
            fun: "getInfo",
            openId: res.data.openId,
            sessionKey: res.data.sessionKey,
          },
          method: "GET",
          success: function (res) {
            if (res.statusCode == 200 && res.data.status == 200) {
              let strMoneyNum = res.data.data.money.toString();
              that.monenyDealNum(strMoneyNum)
            } else {
              console.log(res)
            }
          }

        })
      },
    })
  },


  //将钱转化
  monenyDealNum: function (strMoney){
    if (strMoney.length == 1) {
      strMoney = '0.0' + strMoney;
    } else if (strMoney.length == 2) {
      strMoney = '0.' + strMoney;
    } else {
      strMoney = strMoney.slice(0, strMoney.length - 2) + '.' + strMoney.slice(strMoney.length - 2);
    }
    this.setData({
      mymoney: strMoney,
      outVal: '',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  moneyOutInput: function(e){
    let val = e.detail.value.replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3');
    if (val - 0 < 1 && e.detail.value !='') {
      wx.showModal({
        title: '提示',
        content: '提现金额应大于1元，并且是整数，亲',
        showCancel: false
      })

    }
    this.setData({
      outVal: val,
    })
  },
  selectAll: function(){
    if(this.data.mymoney-0<1){
      wx.showModal({
        title: '提示',
        content: '账户余额不足，不能提现，每次提现最小额度1元，并且为整数。',
        showCancel: false
      })
      return;
    }
    this.setData({
      outVal: parseInt(this.data.mymoney) 
    })
  },
  //提现
  extractMoney:function(){
    let that = this;
    if (that.data.outVal==''){
      wx.showModal({
        title: '提示',
        content: '请输入提现金额',
        showCancel: false
      })
      return;
    }
    if (that.data.outVal - 0 > that.data.mymoney - 0) {
      wx.showModal({
        title: '提示',
        content: '亲，你没有那么多钱',
        showCancel: false
      })
      return;
    }
    wx.showLoading({
      title: '提现中...',
    })
    wx.getStorage({
      key: "3rd_session",
      success: function (res) {
        wx.request({
          url: http + "/wechat/account/user",
          data: {
            fun: "withdraw",
            openId: res.data.openId,
            sessionKey: res.data.sessionKey,
            money: that.data.outVal
          },
          method: "GET",
          success: function (res) {
            wx.hideLoading();
            if (res.statusCode == 200 && res.data.status == 200) {
              //提现成功
              wx.showModal({
                title: '提现成功',
                content: '将在1-5个工作日到账',
                showCancel: false
              })
              let strMoneyNum = (Number(that.data.mymoney.toString().replace('.', '')) - Number(that.data.outVal)*100).toString();

              that.monenyDealNum(strMoneyNum)

            } else {
              console.log(res);
              wx.showModal({
                title: '提现失败',
                content: res.data.msg,
                showCancel: false
              })
            }
          },
          fail: function(){
            wx.showModal({
              title: '提示',
              content: '网络不好，请稍后重试...',
              showCancel: false
            })
          }

        })
      },
    })
  }
})